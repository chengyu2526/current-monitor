#测试rfstar模型更新一次数据耗电量
import serial as s
import time
#open serial port
port = s.Serial(port='COM4',baudrate=115200,timeout=1)
okey=b"OK\r\n"
#唤醒
at=b"+"
port.write(at)
time.sleep(0.01)
#更新数据
at=b"AT+ADV_EXT=40,5000\r\n"
advext_data = b"1234567890123456789012345678901234567890"
port.write(at)
data = port.readline()
# print("ADV_EXT response:",data)
if(data==okey):
    data = port.readline()
    # print("ADV_EXT response:", data)
    port.write(advext_data)
    data = port.readline()
    # print("write data response:",data)
#休眠
at=b"AT+SLEEP=0,1,1\r\n"
port.write(at)
data = port.readline()
# print(data)
#close port
port.close()
print('finish')